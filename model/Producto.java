package model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.NamedStoredProcedureQueries;
import org.eclipse.persistence.annotations.NamedStoredProcedureQuery;

@Entity
@Table(name="PRODUCTO")
@NamedQueries({
	@NamedQuery(name="producto.todos", query="SELECT p FROM Producto p ORDER BY p.nombre ASC"),
	@NamedQuery(name="producto.lastID", query="SELECT MAX(p.id) FROM Producto p")
})
@NamedStoredProcedureQueries({
//	@NamedStoredProcedureQuery(name="PR_INFORMEXDIA")
})

public class Producto {
	@Id
	@Column(nullable=false)
	int id;
	@Column(nullable=false, length=40)
	String nombre;
	@Column(nullable=true)
	String descripcion;
	@Column(nullable=true)
	String categoria;
	@Column(nullable=true)
	float precio_costo;
	@Column(nullable=true)
	float precio_venta;
	@Column(nullable=true)
	float multiplicador;
	@Column(nullable=true, length=40)
	String distribuidor;
	
	//toString
	@Override
	public String toString() {
		return "Producto [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", categoria=" + categoria
				+ ", precio_costo=" + precio_costo + ", precio_venta=" + precio_venta + ", multiplicador=" + multiplicador + ", distribuidor=" + distribuidor + "]";
	}
	
	//Constructor
	public Producto(){
	}
	

	public Producto(int id, String nombre, String descripcion, String categoria, float precio_costo,
			float precio_venta, float multiplicador, String distribuidor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.precio_costo = precio_costo;
		this.precio_venta = precio_venta;
		this.multiplicador = multiplicador;
		this.distribuidor = distribuidor;
	}


	//Getters & Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public float getPrecio_costo() {
		return precio_costo;
	}

	public void setPrecio_costo(float precio_costo) {
		this.precio_costo = precio_costo;
	}

	public float getPrecio_venta() {
		return precio_venta;
	}

	public void setPrecio_venta(float precio_venta) {
		this.precio_venta = precio_venta;
	}
	
	public float getMultiplicador(){
		return multiplicador;
	}
	
	public void setMultipliador(float multiplicador){
		this.multiplicador = multiplicador;
	}

	public String getDistribuidor() {
		return distribuidor;
	}

	public void setDistribuidor(String distribuidor) {
		this.distribuidor = distribuidor;
	}
	
}
