package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InformeMensual {
	@Id
	@Column(nullable=false)
	int id;
	@Column(name="mymes", nullable=false)
	int mes;
	@Column(name="myano", nullable=false)
	int ano;
	@Column(name="mytotal", nullable=false)
	float total;
	@Column(name="myganancia", nullable=false)
	float ganancia;
	

	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getGanancia() {
		return ganancia;
	}
	public void setGanancia(float ganancia) {
		this.ganancia = ganancia;
	}
	public InformeMensual(int mes, int ano, float total, float ganancia) {
		super();
		this.mes = mes;
		this.ano = ano;
		this.total = total;
		this.ganancia = ganancia;
	}
	
	public InformeMensual(){
		
	}
	@Override
	public String toString() {
		return "Informe [id=" + id + ", mes=" + mes + ", ano=" + ano + ", total=" + total
				+ ", ganancia=" + ganancia + "]";
	}
}
