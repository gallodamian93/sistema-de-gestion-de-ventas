package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;

/**
@NamedStoredProcedureQuery(
		name = "PR_INFORMEXDIA", 
		procedureName = "PR_INFORMEXDIA", 
		resultClasses = InformeDiario.class, 
		parameters = {}
	)
*/
@Entity
public class InformeDiario {
	@Id
	@Column(nullable=false)
	int id;
	@Column(name="mydia", nullable=false)
	int dia;
	@Column(name="mymes", nullable=false)
	int mes;
	@Column(name="myano", nullable=false)
	int ano;
	@Column(name="mytotal", nullable=false)
	float total;
	@Column(name="myganancia", nullable=false)
	float ganancia;
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getGanancia() {
		return ganancia;
	}
	public void setGanancia(float ganancia) {
		this.ganancia = ganancia;
	}
	public InformeDiario(int dia, int mes, int ano, float total, float ganancia) {
		super();
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
		this.total = total;
		this.ganancia = ganancia;
	}
	
	public InformeDiario(){
		
	}
	@Override
	public String toString() {
		return "Informe [id=" + id + ", dia=" + dia + ", mes=" + mes + ", ano=" + ano + ", total=" + total
				+ ", ganancia=" + ganancia + "]";
	}
	
}
