package model;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.eclipse.persistence.queries.StoredProcedureCall;
import org.firebirdsql.jdbc.FBObjectListener.ResultSetListener;

public class Util {
	private static EntityManagerFactory emf = null;
	
	public static void close(){
		if (emf != null){
			emf.close();
		};
	}
	
	public static Boolean initEmf(String puName){	
        try {
        	emf = Persistence.createEntityManagerFactory(puName);
        	return true;
        } catch (Throwable ex) {
            return false;
        }
	}

	public static Producto getProducto(int id){
		EntityManager em = emf.createEntityManager();
		return em.find(Producto.class, id);
	}
	
	public static void updateProducto(Producto actualizado){
		EntityManager em = emf.createEntityManager();
		Producto old = em.find(Producto.class, actualizado.getId());
		em.getTransaction().begin();
		
		old.setNombre(actualizado.getNombre());
		old.setDescripcion(actualizado.getDescripcion());
		old.setCategoria(actualizado.getCategoria());
		old.setDistribuidor(actualizado.getDistribuidor());
		old.setPrecio_costo(actualizado.getPrecio_costo());
		old.setMultipliador(actualizado.getMultiplicador());
		old.setPrecio_venta(actualizado.getPrecio_venta());
		
		em.getTransaction().commit();
		em.close();
	}
	
	public static void updatePedido(Pedido actualizado){
		EntityManager em = emf.createEntityManager();
		Pedido old = em.find(Pedido.class, actualizado.getId());
		em.getTransaction().begin();
		
		old.setDia(actualizado.getDia());
		old.setMes(actualizado.getMes());
		old.setAno(actualizado.getAno());
		old.setTotal(actualizado.getTotal());
		old.setGanancia(actualizado.getGanancia());
		
		em.getTransaction().commit();
		em.close();
	}
	
	public static String insProducto(Producto p){
		String errno;
		EntityManager em = emf.createEntityManager();
		EntityTransaction emt = em.getTransaction();
		emt.begin();
		try {
			em.persist(p);
			emt.commit();
			errno = "OK";
			
		} catch (Exception e) {
			errno = "ERROR";
		}
		em.close();
		return errno;
	}
	
	public static String insDetalle(Detalle d){
		String errno;
		EntityManager em = emf.createEntityManager();
		EntityTransaction emt = em.getTransaction();
		emt.begin();
		try {
			em.persist(d);
			emt.commit();
			errno = "OK";
			
		} catch (Exception e) {
			errno = "ERROR";
		}
		em.close();
		return errno;
	}
	
	public static String insPedido(Pedido p){
		String errno;
		EntityManager em = emf.createEntityManager();
		EntityTransaction emt = em.getTransaction();
		emt.begin();
		try {
			em.persist(p);
			emt.commit();
			errno = "OK";
			
		} catch (Exception e) {
			errno = "ERROR";
		}
		em.close();
		return errno;
	}
	
	public static List<Producto> getAllProductos(){
		EntityManager em = emf.createEntityManager();
		Query qry = em.createNamedQuery("producto.todos");
		return qry.getResultList();
	}
	
	public static List<ProductoSimple> productosVendidosDia(){
		EntityManager em = emf.createEntityManager();
		List<ProductoSimple> result = em.createNativeQuery("select * from pr_ventasdeldia", ProductoSimple.class).getResultList();
		em.clear();
		em.close();
		return result;
	}

	public static List<ProductoSimple> productosVendidosMes(){
		EntityManager em = emf.createEntityManager();
		List<ProductoSimple> result = em.createNativeQuery("select * from pr_ventasdelMes", ProductoSimple.class).getResultList();
		em.clear();
		em.close();
		return result;
	}
	
	public static List<ProductoSimple> productosVendidosA�o() {
		EntityManager em = emf.createEntityManager();
		List<ProductoSimple> result = em.createNativeQuery("select * from pr_ventasdelano", ProductoSimple.class).getResultList();
		em.clear();
		em.close();
		return result;
	}

	public static List<model.InformeDiario> getInformeDiario(){
		EntityManager em = emf.createEntityManager();
		List<model.InformeDiario> result = em.createNativeQuery("select * from pr_informexdia", InformeDiario.class).getResultList();
		em.clear();
		em.close();
	    return result;
	}
	
	public static List<model.InformeMensual> getInformeMensual() {
		EntityManager em = emf.createEntityManager();
		List<model.InformeMensual> result = em.createNativeQuery("select * from pr_informexmes", InformeMensual.class).getResultList();
		em.clear();
		em.close();
	    return result;
	}
	
	public static List<InformeAnual> getInformeAnual() {
		EntityManager em = emf.createEntityManager();
		List<model.InformeAnual> result = em.createNativeQuery("select * from pr_informexano", InformeAnual.class).getResultList();
		em.clear();
		em.close();
	    return result;
	}
	
	public static List<Venta> getUltimaVenta(){
		EntityManager em = emf.createEntityManager();
		List<Venta> result = em.createNativeQuery("select * from pr_ultimaventa", Venta.class).getResultList();
		em.clear();
		em.close();
		return result;
	}
	
	public static int nextIdProducto(){
		EntityManager em = emf.createEntityManager();
		Query qry = em.createNamedQuery("producto.lastID");
		int response = 0;
		try {
			response = (int)qry.getSingleResult();
			em.clear();
			em.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response + 1;
	}
	
	public static int nextIdPedido(){
		EntityManager em = emf.createEntityManager();
		Query qry = em.createNamedQuery("pedido.lastID");
		int response = 0;
		try {
			response = (int)qry.getSingleResult();
			em.clear();
			em.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response + 1;
	}
	
	public static List<Pedido> getPedidos(String rango){
		EntityManager em = emf.createEntityManager();
		List<Pedido> lista;
		switch (rango) {
		case "DIA":
			lista = em.createNativeQuery("select * from pr_pedidos('DIA')", Pedido.class).getResultList();
			em.clear();
			em.close();
			return lista;
		case "MES":
			lista = em.createNativeQuery("select * from pr_pedidos('MES')", Pedido.class).getResultList();
			em.clear();
			em.close();
			return lista;
		case "A�O":
			lista = em.createNativeQuery("select * from pr_pedidos('ANO')", Pedido.class).getResultList();
			em.clear();
			em.close();
			return lista;
		default:
			em.clear();
			em.close();
			return null;
		}	
	}
	
	public static List<model.ProductoSimple> getDetalle(int id){
		EntityManager em = emf.createEntityManager();
		Query qry = em.createNativeQuery("select * from pr_detalles(?1)", ProductoSimple.class);
		qry.setParameter("1", id);
		List<model.ProductoSimple> lista = qry.getResultList();
		em.clear();
		em.close();
		return lista;
	}
}
