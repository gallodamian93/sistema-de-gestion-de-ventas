package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="PEDIDO")
@NamedQueries({
	@NamedQuery(name="pedido.todos", query="SELECT p FROM Pedido p"),
	@NamedQuery(name="pedido.lastID", query="SELECT MAX(p.id) FROM Pedido p")
})
public class Pedido {

	@Id
	@Column(nullable=false)
	int id;
	@Column(nullable=false)
	int dia;
	@Column(nullable=true)
	int mes;
	@Column(nullable=true)
	int ano;
	@Column(nullable=true)
	float total;
	@Column(nullable=true)
	float ganancia;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getGanancia() {
		return ganancia;
	}
	public void setGanancia(float ganancia) {
		this.ganancia = ganancia;
	}
	public Pedido(int id, int dia, int mes, int ano, float total, float ganancia) {
		super();
		this.id = id;
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
		this.total = total;
		this.ganancia = ganancia;
	}
		
	public Pedido(){
		
	}
	
}
