package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.ImageIcon;

import net.miginfocom.swing.MigLayout;
import utils.FormatRenderer;
import utils.NumberRenderer;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import model.Detalle;
import model.Pedido;
import model.Producto;
import model.Util;
import model.Venta;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.RowFilter;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDate;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.JSeparator;
import java.awt.Label;
import javax.swing.border.MatteBorder;
import java.awt.Font;
import java.awt.Frame;

@SuppressWarnings("serial")
public class Home extends JFrame{

	private JPanel contentPane;
	private JTable table;
	private JButton btnNuevo;
	private JButton btnModificar;
	private JTextField txtBuscar;
	private JScrollPane scrollPane;
	private JTable tbl_pedido;
	private JScrollPane scrollPane_1;
	private JButton btnAgregar;
	private JButton btnQuitar;
	private JTextField txt_total;
	private JLabel lblTotal;
	private JButton btnConfirmar;
	private JButton btnLimpiar;
	private JMenuBar menuBar;
	private JMenu mnProductos;
	private JLabel lblBuscar;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmInformeMensual;
	private JLabel lblProductos;
	private JLabel lblVenta;
	private JSeparator separator;
	private JLabel lblltimaVenta;
	private JTable tbl_ultima;
	private JScrollPane scrollPane_2;
	private Label lbl_total_ult;
	private JTextField txt_total_ult;
	private JPanel panel_logo;
	private JMenuItem mntmInformeAnual;
	private JMenu mnVentas;
	private JMenuItem mntmBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setTitle("Sistema de gesti\u00F3n de ventas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 924, 744);
		setLocationRelativeTo(null);
		
		try {
		    ImageIcon img = new ImageIcon("img/ico.png");
		    setIconImage(img.getImage());
		} catch (Exception e) {
			// TODO: handle exception
		}

		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnVentas = new JMenu("Ventas");
		menuBar.add(mnVentas);
		
		mntmBuscar = new JMenuItem("Historial");
		mntmBuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				Ventas v = new Ventas();
				v.setVisible(true);
			}
		});
		mnVentas.add(mntmBuscar);
		
		mnProductos = new JMenu("Informes");
		menuBar.add(mnProductos);
		
		mntmNewMenuItem = new JMenuItem("Informe diario");
		mntmNewMenuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					Informe i = new Informe("d�a");
					i.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error inesperado");
				}
			}
		});
		mntmNewMenuItem.setHorizontalAlignment(SwingConstants.LEFT);
		mnProductos.add(mntmNewMenuItem);
		
		mntmInformeMensual = new JMenuItem("Informe mensual");
		mntmInformeMensual.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					Informe i = new Informe("mes");
					i.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error inesperado");
				}
			}
		});
		mntmInformeMensual.setHorizontalAlignment(SwingConstants.LEFT);
		mnProductos.add(mntmInformeMensual);
		
		mntmInformeAnual = new JMenuItem("Informe anual");
		mntmInformeAnual.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					Informe i = new Informe("a�o");
					i.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error inesperado");
				}
			}
		});
		mnProductos.add(mntmInformeAnual);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[400px:n,grow][][400px:n,grow]", "[][][150px:150px:150px][50px:50px:50px][50px:50px:50px][150px:n,grow][][][][][100px:100px:100px][]"));
		
		lblVenta = new JLabel("Venta");
		lblVenta.setForeground(Color.GRAY);
		contentPane.add(lblVenta, "cell 2 0");
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 1 1 5,grow");
		
		table = new JTable();
		table.setShowHorizontalLines(false);
		table.setShowGrid(false);
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "PRODUCTO", "CATEGORIA", "PRECIO"
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Float.class
			};
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(25);
		table.getColumnModel().getColumn(1).setPreferredWidth(175);
		table.getColumnModel().getColumn(2).setPreferredWidth(90);
		table.getColumnModel().getColumn(3).setPreferredWidth(40);
		
		table.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		table.getColumnModel().getColumn(3).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		
		/*
		 *
		TableColumnModel m = table.getColumnModel();
		m.getColumn(0).setCellRenderer(FormatRenderer.getDateTimeRenderer());
		m.getColumn(1).setCellRenderer(FormatRenderer.getTimeRenderer());
		m.getColumn(2).setCellRenderer(NumberRenderer.getPercentRenderer());
		m.getColumn(3).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		 * 
		 * 
		 */
		
		
	    table.addKeyListener(new KeyListener() {
	        @Override
	        public void keyTyped(KeyEvent e) {
	        }

	        @Override
	        public void keyPressed(KeyEvent e) {
	        	if (e.getKeyCode() == 10) {
					agregarAVenta();
				}
	        }

	        @Override
	        public void keyReleased(KeyEvent e) {
	        }
	    });
		
		lblProductos = new JLabel("Productos");
		lblProductos.setForeground(Color.GRAY);
		contentPane.add(lblProductos, "flowx,cell 0 0");
		
		btnAgregar = new JButton(">>>");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarAVenta();
			}
		});
		contentPane.add(btnAgregar, "flowx,cell 1 3,grow");
		
		scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "cell 2 1 1 5,grow");
		
		tbl_pedido = new JTable();
		tbl_pedido.setShowHorizontalLines(false);
		tbl_pedido.setShowGrid(false);
		
		tbl_pedido.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "PRODUCTO", "PRECIO", "CANTIDAD", "SUBTOTAL"
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Float.class, Float.class, Float.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		tbl_pedido.getColumnModel().getColumn(0).setPreferredWidth(25);
		tbl_pedido.getColumnModel().getColumn(1).setPreferredWidth(175);
		tbl_pedido.getColumnModel().getColumn(2).setPreferredWidth(40);
		tbl_pedido.getColumnModel().getColumn(3).setPreferredWidth(50);
		tbl_pedido.getColumnModel().getColumn(4).setPreferredWidth(50);
		
		tbl_pedido.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		tbl_pedido.getColumnModel().getColumn(2).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		tbl_pedido.getColumnModel().getColumn(4).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		
		TableModelListener t = new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				tbl_pedido.getModel().removeTableModelListener(this);
				//actualizo subtotal de la fila
				if((e.getType() == 0) && (e.getColumn() == 3)){
					int row = tbl_pedido.getSelectedRow();
	                float cantidad = (float) tbl_pedido.getValueAt(row, 3);
	                float subtotal = cantidad * (float) tbl_pedido.getValueAt(row, 2);
	                tbl_pedido.getModel().setValueAt(subtotal, row, 4);
				} else if((e.getType() == 0) && (e.getColumn() == 4)){
					int row = tbl_pedido.getSelectedRow();
					float subtotal = (float) tbl_pedido.getValueAt(row, 4);
					float cantidad = (float) subtotal/(float)tbl_pedido.getValueAt(row, 2);
					tbl_pedido.getModel().setValueAt(cantidad, row, 3);
				}
				//actualizo total de la venta
				updateTotal();
				tbl_pedido.getModel().addTableModelListener(this);				
			}
		};
		tbl_pedido.getModel().addTableModelListener(t);
		/*
		 tbl_pedido.addKeyListener(new KeyAdapter() {
	        @Override
	        public void keyPressed(KeyEvent e) {
	            if (e.getKeyCode() == KeyEvent.VK_ENTER) {

	                int row = tbl_pedido.getSelectedRow();

	                float cantidad = (float) tbl_pedido.getValueAt(row, 3);
	                
	                float subtotal = cantidad * (float) tbl_pedido.getValueAt(row, 2);

	                tbl_pedido.getModel().setValueAt(subtotal, row, 4);

	            }
	        }
	    });*/
		scrollPane_1.setViewportView(tbl_pedido);
		
		btnQuitar = new JButton("<<<");
		btnQuitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model_pedido = (DefaultTableModel) tbl_pedido.getModel();
				try {
					model_pedido.removeRow(tbl_pedido.getSelectedRow());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Seleccione el producto que desea quitar del pedido");
				}
				
			}
		});
		contentPane.add(btnQuitar, "cell 1 4,grow");
		
		txtBuscar = new JTextField();
		txtBuscar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				DefaultTableModel table2 = (DefaultTableModel) table.getModel();
				String search = txtBuscar.getText().toUpperCase();
				TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table2);
				table.setRowSorter(tr);
				tr.setRowFilter(RowFilter.regexFilter(search));
			}
		});
		
		lblBuscar = new JLabel("Buscar:");
		contentPane.add(lblBuscar, "flowx,cell 0 6");
		contentPane.add(txtBuscar, "cell 0 6,growx");
		txtBuscar.setColumns(10);
		
		lblTotal = new JLabel("Total");
		contentPane.add(lblTotal, "flowx,cell 2 6,alignx right");
		
		txt_total = new JTextField();
		txt_total.setFont(new Font("Tahoma", Font.BOLD, 11));
		txt_total.setEditable(false);
		contentPane.add(txt_total, "cell 2 6,alignx right");
		txt_total.setColumns(10);

		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int input = JOptionPane.showConfirmDialog(null, "Est� seguro que desea cancelar la venta?");
				if(input == 0){
					DefaultTableModel model = (DefaultTableModel) tbl_pedido.getModel();
					model.setRowCount(0);
				}
			}
		});

		Home me = this;
		btnNuevo = new JButton("Nuevo producto");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Carga carga = new Carga(true, 0, me);
					carga.setVisible(true);	
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error inesperado");
				}
			}
		});
		contentPane.add(btnNuevo, "flowx,cell 0 7");
		contentPane.add(btnLimpiar, "flowx,cell 2 7,alignx right");
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) tbl_pedido.getModel();
				if(model.getRowCount() == 0){
					JOptionPane.showMessageDialog(null, "Agregue al menos 1 producto para confirmar el pedido");
				} else {
					try {
						//Inserto pedido vacio
						int newIdPedido = Util.nextIdPedido();
						Pedido pe = new Pedido(newIdPedido, LocalDate.now().getDayOfMonth(), LocalDate.now().getMonthValue(), LocalDate.now().getYear(), 0, 0);
						Util.insPedido(pe);
						//inserto detalles
						float total = 0;
						float ganancia = 0;
						for(int i=0; i < tbl_pedido.getRowCount(); i++){
							int id = (int) model.getValueAt(i, 0);
							float cantidad = (float) model.getValueAt(i, 3);
							Producto pr = Util.getProducto(id);
							total += pr.getPrecio_venta() * cantidad;
							ganancia += ((pr.getPrecio_venta() - pr.getPrecio_costo()) * cantidad);
							Detalle d = new Detalle(newIdPedido, id, cantidad);
							Util.insDetalle(d);
						}
						//actualizo pedido
						pe.setTotal(total);
						pe.setGanancia(ganancia);
						Util.updatePedido(pe);
						JOptionPane.showMessageDialog(null, "Pedido registrado con �xito!");
						model.setRowCount(0);
						//actualizo "ultima venta"
						updateUltima();

					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Error: intente nuevamente");
					}
				}
			}
		});
		contentPane.add(btnConfirmar, "cell 2 7,alignx right");
		
		btnModificar = new JButton("Modificar producto");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Carga carga = new Carga(false, (int)table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0), me);
					carga.setVisible(true);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		});
		contentPane.add(btnModificar, "cell 0 7");
		
		separator = new JSeparator();
		separator.setForeground(Color.GRAY);
		contentPane.add(separator, "cell 0 8 3 1,growx");
		
		lblltimaVenta = new JLabel("\u00DAltima venta");
		lblltimaVenta.setForeground(Color.GRAY);
		contentPane.add(lblltimaVenta, "cell 0 9");
		
		scrollPane_2 = new JScrollPane();
		contentPane.add(scrollPane_2, "cell 0 10,grow");
		
		tbl_ultima = new JTable();
		tbl_ultima.setShowHorizontalLines(false);
		tbl_ultima.setShowGrid(false);
		tbl_ultima.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID", "PRODUCTO", "PRECIO", "CANTIDAD", "SUBTOTAL"
				}
			) {
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, Float.class, Float.class, Float.class
				};
				@SuppressWarnings({ "rawtypes", "unchecked" })
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		tbl_ultima.getColumnModel().getColumn(0).setPreferredWidth(25);
		tbl_ultima.getColumnModel().getColumn(1).setPreferredWidth(175);
		tbl_ultima.getColumnModel().getColumn(2).setPreferredWidth(40);
		tbl_ultima.getColumnModel().getColumn(3).setPreferredWidth(50);
		tbl_ultima.getColumnModel().getColumn(4).setPreferredWidth(50);

		tbl_ultima.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		tbl_ultima.getColumnModel().getColumn(2).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		tbl_ultima.getColumnModel().getColumn(4).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		
		scrollPane_2.setViewportView(tbl_ultima);
		
		panel_logo = new JPanel();
		panel_logo.setBackground(Color.WHITE);
		panel_logo.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		contentPane.add(panel_logo, "cell 2 10,grow");
		loadLogo();
		
		lbl_total_ult = new Label("Total:");
		contentPane.add(lbl_total_ult, "flowx,cell 0 11,alignx right");
		
		txt_total_ult = new JTextField();
		txt_total_ult.setFont(new Font("Tahoma", Font.BOLD, 11));
		txt_total_ult.setEnabled(false);
		txt_total_ult.setEditable(false);
		contentPane.add(txt_total_ult, "cell 0 11,alignx right");
		txt_total_ult.setColumns(10);
		try {
			load();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al cargar los productos");
		}
		setVisible(true);
	}
	
	private void loadLogo() {
		try {
			panel_logo.add(new JLabel(new ImageIcon("img/logo-minimo.png")));
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

	public void load(){
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		List<Producto> lista = Util.getAllProductos();
		Object rowData[] = new Object[4];
		for(int i=0; i < lista.size(); i++){
			rowData[0] = lista.get(i).getId();
			rowData[1] = lista.get(i).getNombre();
			rowData[2] = lista.get(i).getCategoria();
			rowData[3] = lista.get(i).getPrecio_venta();
			model.addRow(rowData);
		}
	}
	
	private void updateTotal(){
		DefaultTableModel model = (DefaultTableModel) tbl_pedido.getModel();		
		float acumulador = 0;
		for (int i=0;i < model.getRowCount(); i++){
			acumulador += (float)model.getValueAt(i, 4);
		}
		txt_total.setText(Float.toString(acumulador));
	}
	
	private void updateUltima(){
		DefaultTableModel model = (DefaultTableModel) tbl_ultima.getModel();
		model.setRowCount(0);
		List<Venta> lista = Util.getUltimaVenta();
		Object rowData[] = new Object[5];
		for(int i=0; i<lista.size();i++){
			rowData[0] = lista.get(i).getId();
			rowData[1] = lista.get(i).getNombre();
			rowData[2] = lista.get(i).getPrecio();
			rowData[3] = lista.get(i).getCantidad();
			rowData[4] = (float)rowData[2] * (float)rowData[3];
			model.addRow(rowData);
		}
		txt_total_ult.setText(Float.toString(lista.get(0).getTotal()));
		
	}
	
	private void agregarAVenta(){
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Object rowData[] = new Object[5];
		try {
			rowData[0] = (int) model.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0);
			rowData[1] = (String) model.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 1);
			rowData[2] = (float) model.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 3);
			rowData[3] = 1.0f;
			rowData[4] = rowData[2];
			DefaultTableModel model_pedido = (DefaultTableModel) tbl_pedido.getModel();
			model_pedido.addRow(rowData);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Seleccione el producto que desea agregar al pedido");
		}
	}

}
