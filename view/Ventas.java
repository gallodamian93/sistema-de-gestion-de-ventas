package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import model.InformeDiario;
import model.Util;
import net.miginfocom.swing.MigLayout;
import utils.FormatRenderer;
import utils.NumberRenderer;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import java.awt.Choice;
import java.awt.Label;
import java.awt.TextField;
import javax.swing.JTextField;

public class Ventas extends JFrame {

	private JPanel contentPane;
	private JTable tbl_venta;
	private JTable tbl_ventas;
	private JLabel lblVerPedidosDe;
	private JLabel lblPedidos;
	private Choice choice;
	ListSelectionListener listSelListener;
	private JLabel lblDetalles;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventas frame = new Ventas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventas() {
		setTitle("Historial de ventas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 564, 509);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][][grow][][]", "[][][][][grow][][]"));
		
		try {
		    ImageIcon img = new ImageIcon("img/ico.png");
		    setIconImage(img.getImage());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		lblVerPedidosDe = new JLabel("Ver ventas de");
		contentPane.add(lblVerPedidosDe, "flowx,cell 0 0");
		
		lblPedidos = new JLabel("Seleccione una venta para ver los detalles");
		lblPedidos.setForeground(Color.GRAY);
		contentPane.add(lblPedidos, "cell 0 1");
		
		lblDetalles = new JLabel("Detalles");
		lblDetalles.setForeground(Color.GRAY);
		contentPane.add(lblDetalles, "cell 4 1");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "cell 0 2 3 1,grow");
		
		tbl_ventas = new JTable();
		tbl_ventas.setShowHorizontalLines(false);
		tbl_ventas.setShowGrid(false);
		tbl_ventas.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID", "FECHA", "TOTAL"
				}
			) {
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, Float.class
				};
				@SuppressWarnings({ "rawtypes", "unchecked" })
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		tbl_ventas.getColumnModel().getColumn(0).setPreferredWidth(15);
		tbl_ventas.getColumnModel().getColumn(1).setPreferredWidth(40);
		tbl_ventas.getColumnModel().getColumn(2).setPreferredWidth(40);
		
		tbl_ventas.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		tbl_ventas.getColumnModel().getColumn(2).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		scrollPane_1.setViewportView(tbl_ventas);
		
		listSelListener = new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int id = (int) tbl_ventas.getValueAt(tbl_ventas.getSelectedRow(), 0);
				try {
					populateVenta(id);
				} catch (Exception e2) {
					e2.printStackTrace();
					JOptionPane.showMessageDialog(null, "Error al cargar los detalles del pedido seleccionado");
				}
			}
		};
		
		tbl_ventas.getSelectionModel().addListSelectionListener(listSelListener);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 4 2,grow");
		
		tbl_venta = new JTable();
		scrollPane.setViewportView(tbl_venta);
		tbl_venta.setShowHorizontalLines(false);
		tbl_venta.setShowGrid(false);
		tbl_venta.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID", "PRODUCTO", "CANTIDAD",
				}
			) {
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, Float.class
				};
				@SuppressWarnings({ "rawtypes", "unchecked" })
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		tbl_venta.getColumnModel().getColumn(0).setPreferredWidth(25);
		tbl_venta.getColumnModel().getColumn(1).setPreferredWidth(175);
		tbl_venta.getColumnModel().getColumn(2).setPreferredWidth(40);
		
		tbl_venta.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		
		choice = new Choice();
		choice.add("hoy");
		choice.add("este mes");
		choice.add("este a�o");
		
		choice.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				tbl_ventas.getSelectionModel().removeListSelectionListener(listSelListener);
				switch (choice.getSelectedItem()) {
				case "hoy":
					try {
						populateVentas("DIA");
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ventas");
					}
					break;
				case "este mes":
					try {
						populateVentas("MES");
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ventas");
					}
					break;
				case "este a�o":
					try {
						populateVentas("A�O");
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ventas");
					}
					break;
				default:
					break;
				}
				tbl_ventas.getSelectionModel().addListSelectionListener(listSelListener);
			}
		});
		contentPane.add(choice, "cell 0 0");

		
		try {
			populateVentas("DIA");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ventas");
		}
		
		
	}
	
	public void populateVentas(String rango){
		DefaultTableModel model = (DefaultTableModel) tbl_ventas.getModel();
		//tbl_ventas.getSelectionModel().removeListSelectionListener(tbl_ventas);
		model.setRowCount(0);
		List<model.Pedido> lista = Util.getPedidos(rango);
		Object rowData[] = new Object[3];
		for(int i=0; i<lista.size(); i++){
			rowData[0] = lista.get(i).getId();
			rowData[1] = Integer.toString(((model.Pedido) lista.get(i)).getDia()) + "/" + Integer.toString(((model.Pedido) lista.get(i)).getMes()) + "/" + Integer.toString(((model.Pedido) lista.get(i)).getAno());
			rowData[2] = lista.get(i).getTotal();
			model.addRow(rowData);
		}
		//tbl_ventas.getSelectionModel().addListSelectionListener(listSelListener);
	}
	
	public void populateVenta(int id){
		DefaultTableModel model = (DefaultTableModel) tbl_venta.getModel();
		model.setRowCount(0);
		List<model.ProductoSimple> lista = Util.getDetalle(id);
		Object rowData[] = new Object[3];
		for(int i=0; i<lista.size(); i++){
			rowData[0] = lista.get(i).getId();
			rowData[1] = lista.get(i).getNombre();
			rowData[2] = lista.get(i).getCantidad();
			model.addRow(rowData);
		}
	}

}
