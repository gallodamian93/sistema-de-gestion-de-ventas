package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Producto;
import model.Util;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class Carga extends JFrame {

	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtNombre;
	private JTextField txtDescr;
	private JTextField txtCosto;
	private JTextField txtVenta;
	private JTextField txtDistribuidor;
	private JLabel lblDistribuidor;
	private JTextField txtMulti;
	private JLabel lblMultiplicador;
	private JButton btnCalcular;
	private JComboBox comboBox;
	private static Home home;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Carga frame = new Carga(true, 0, home);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Carga(boolean esNuevo, int id, Home home) {
		setTitle("Ficha de producto");
		this.home = home;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(400, 100, 538, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[139.00][grow]", "[][][][][][][][][][][]"));
		
		try {
		    ImageIcon img = new ImageIcon("img/ico.png");
		    setIconImage(img.getImage());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		JLabel lblId = new JLabel("ID");
		contentPane.add(lblId, "cell 0 1,alignx left");
		
		txtId = new JTextField();
		txtId.setEditable(false);
		contentPane.add(txtId, "cell 1 1,growx");
		txtId.setColumns(10);
		int idPantalla = (esNuevo) ? Util.nextIdProducto(): id;
		txtId.setText(String.valueOf(idPantalla));
		
		JLabel lblNombre = new JLabel("Nombre");
		contentPane.add(lblNombre, "cell 0 2,alignx left");
		
		txtNombre = new JTextField();
		contentPane.add(txtNombre, "cell 1 2,growx");
		txtNombre.setColumns(10);
		
		JLabel lblDescipcin = new JLabel("Descipci\u00F3n");
		contentPane.add(lblDescipcin, "cell 0 3,alignx left");
		
		txtDescr = new JTextField();
		contentPane.add(txtDescr, "cell 1 3,growx");
		txtDescr.setColumns(10);
		
		JLabel lblCategora = new JLabel("Categor\u00EDa");
		contentPane.add(lblCategora, "cell 0 4,alignx left");
		
		lblDistribuidor = new JLabel("Distribuidor");
		contentPane.add(lblDistribuidor, "cell 0 5,alignx left");
		
		txtDistribuidor = new JTextField();
		contentPane.add(txtDistribuidor, "cell 1 5,growx");
		txtDistribuidor.setColumns(10);
		
		JLabel lblPrecioDeCosto = new JLabel("Precio de costo");
		contentPane.add(lblPrecioDeCosto, "cell 0 6,alignx left");
		
		txtCosto = new JTextField();
		contentPane.add(txtCosto, "cell 1 6,growx");
		txtCosto.setColumns(10);
		
		lblMultiplicador = new JLabel("Multiplicador");
		contentPane.add(lblMultiplicador, "cell 0 7,alignx left");
		
		txtMulti = new JTextField();
		contentPane.add(txtMulti, "cell 1 7,growx");
		txtMulti.setColumns(10);
		
		JLabel lblPrecioDeVenta = new JLabel("Precio de venta");
		contentPane.add(lblPrecioDeVenta, "cell 0 8,alignx left");
		
		txtVenta = new JTextField();
		contentPane.add(txtVenta, "flowx,cell 1 8,growx");
		txtVenta.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Producto p = new Producto(Integer.valueOf(txtId.getText()), txtNombre.getText().toUpperCase(), txtDescr.getText().toUpperCase(), comboBox.getSelectedItem().toString(), Float.valueOf(txtCosto.getText()), Float.valueOf(txtVenta.getText()), Float.valueOf(txtMulti.getText()), txtDistribuidor.getText().toUpperCase());
				
					if(esNuevo){
						Util.insProducto(p);
					} else {
						Util.updateProducto(p);
					}
					
					dispose();
					home.load();
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Error: verifique que los tipos de datos ingresados sean adecuados");
				}

				
			}
		});
		contentPane.add(btnGuardar, "flowx,cell 1 10");
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					txtVenta.setText(String.valueOf(Float.valueOf(txtCosto.getText())* Float.valueOf(txtMulti.getText())));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error: verifique los campos 'precio de costo' y 'multiplicador'");
				}
			}
		});
		contentPane.add(btnCalcular, "cell 1 8");
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"ALIMENTO PERROS", "ALIMENTO GATOS", "ALIMENTO AVES", "ALIMENTO PECES", "ALIMENTO CONEJOS", "ALIMENTO OTROS", "MASCOTAS SALUD", "MASCOTAS ACCESORIOS", "ARTICULOS DE LIMPIEZA", "JARDIN", "VARIOS"}));
		contentPane.add(comboBox, "cell 1 4");
		
		//si es modificar salgo a buscar el producto con el id que me mandaron
		
		if(!esNuevo){
			Producto p = Util.getProducto(id);
			txtNombre.setText(p.getNombre());
			txtDescr.setText(p.getDescripcion());
			//txtCat.setText(p.getCategoria());

			ComboBoxModel model = comboBox.getModel();
			int size = model.getSize();
			for(int i=0;i<size;i++){
                String element = (String) model.getElementAt(i);
                if(element.equals(p.getCategoria())){
                	comboBox.setSelectedIndex(i);
                	break;
                }
			}
			
			txtDistribuidor.setText(p.getDistribuidor());
			txtCosto.setText(String.valueOf(p.getPrecio_costo()));
			txtMulti.setText(String.valueOf(p.getMultiplicador()));
			txtVenta.setText(String.valueOf(p.getPrecio_venta()));
		}
	}

}
