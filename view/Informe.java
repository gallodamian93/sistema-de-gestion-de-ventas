package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import utils.FormatRenderer;
import utils.NumberRenderer;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import model.InformeAnual;
import model.InformeDiario;
import model.InformeMensual;
import model.Util;
import javax.swing.JLabel;
import java.awt.Color;

public class Informe extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JLabel lblGananciaTotalPor;
	private JTable table_vendidos;
	private JScrollPane scrollPane_1;
	private JLabel lblProductosMsVendidos;
	private JButton btnRecargar;
	private String tipo; 


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Informe frame = new Informe("dia");
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Informe(String tipo) {
		this.tipo = tipo;
		setTitle("Informe de ventas por " + tipo);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 647, 441);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		try {
		    ImageIcon img = new ImageIcon("img/ico.png");
		    setIconImage(img.getImage());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		lblGananciaTotalPor = new JLabel("Ganancia total por " + tipo);
		lblGananciaTotalPor.setForeground(Color.GRAY);
		contentPane.add(lblGananciaTotalPor, "cell 0 0");
		
		lblProductosMsVendidos = new JLabel("Productos vendidos en el " + tipo);
		lblProductosMsVendidos.setForeground(Color.GRAY);
		contentPane.add(lblProductosMsVendidos, "cell 1 0");
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 1,grow");
		
		btnRecargar = new JButton("\u21BB");
		btnRecargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					populateTable();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ganancias");
				}
				
				try{
					populateTableVendidos();
				}catch (Exception e){
					JOptionPane.showMessageDialog(null, "Error al cargar la tabla de productos vendidos");
				}
			}
		});
		btnRecargar.setFont(new Font("Lucida Sans Unicode", Font.PLAIN, 13));
		contentPane.add(btnRecargar, "cell 2 0,alignx right");
		
		scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "cell 1 1 2 1,grow");
		
		table_vendidos = new JTable();
		table_vendidos.setEnabled(false);
		table_vendidos.setShowVerticalLines(false);
		table_vendidos.setShowGrid(false);
		scrollPane_1.setViewportView(table_vendidos);
		table_vendidos.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "PRODUCTO", "VENDIDOS"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Float.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table_vendidos.getColumnModel().getColumn(0).setPreferredWidth(25);
		table_vendidos.getColumnModel().getColumn(1).setPreferredWidth(160);
		table = new JTable();
		table.setShowVerticalLines(false);
		table.setShowHorizontalLines(false);
		table.setShowGrid(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				tipo.toUpperCase(), "TOTAL", "GANANCIAS"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, Float.class, Float.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(80);
		table.getColumnModel().getColumn(2).setPreferredWidth(80);
		
		table.getColumnModel().getColumn(0).setCellRenderer(NumberRenderer.getIntegerRenderer());
		table.getColumnModel().getColumn(1).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		table.getColumnModel().getColumn(2).setCellRenderer(NumberRenderer.getCurrencyRenderer());
		
		scrollPane.setViewportView(table);
		table.setEnabled(false);
		try {
			populateTable();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al cargar la tabla de ganancias");
		}
		try{
			populateTableVendidos();
		}catch (Exception e){
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "Error al cargar la tabla de productos vendidos");
		}
		
	}
	private void populateTable(){
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		List lista;
		Object rowData[];
		switch (tipo) {
		case "d�a":
			lista = (List<model.InformeDiario>)Util.getInformeDiario();
			rowData = new Object[3];
			for(int i=0; i<lista.size(); i++){
				rowData[0] = Integer.toString(((InformeDiario) lista.get(i)).getDia()) + "/" + Integer.toString(((InformeDiario) lista.get(i)).getMes()) + "/" + Integer.toString(((InformeDiario) lista.get(i)).getAno());
				rowData[1] = ((InformeDiario) lista.get(i)).getTotal();
				rowData[2] = ((InformeDiario) lista.get(i)).getGanancia();
				model.addRow(rowData);
			}
			break;
		case "mes":
			lista = (List<model.InformeMensual>)Util.getInformeMensual();
			rowData = new Object[3];
			for(int i=0; i<lista.size(); i++){
				rowData[0] = Integer.toString(((InformeMensual) lista.get(i)).getMes()) + "/" + Integer.toString(((InformeMensual) lista.get(i)).getAno());
				rowData[1] = ((InformeMensual) lista.get(i)).getTotal();
				rowData[2] = ((InformeMensual) lista.get(i)).getGanancia();
				model.addRow(rowData);
			}
			break;
		case "a�o":
			lista = (List<model.InformeAnual>)Util.getInformeAnual();
			rowData = new Object[3];
			for(int i=0; i<lista.size(); i++){
				rowData[0] = Integer.toString(((InformeAnual) lista.get(i)).getAno());
				rowData[1] = ((InformeAnual) lista.get(i)).getTotal();
				rowData[2] = ((InformeAnual) lista.get(i)).getGanancia();
				model.addRow(rowData);
			}
			break;
		default:
			break;
		}
	}
	
	private void populateTableVendidos() throws Exception{
		DefaultTableModel model = (DefaultTableModel) table_vendidos.getModel();
		model.setRowCount(0);
		List<model.ProductoSimple> lista = null;
		switch (tipo) {
		case "d�a":
			lista = Util.productosVendidosDia();
			break;
		case "mes":
			lista = Util.productosVendidosMes();
			break;
		case "a�o":
			lista = Util.productosVendidosA�o();
		}
		Object rowData[] = new Object[3];
		for(int i=0; i<lista.size(); i++){
			rowData[0] = lista.get(i).getId();
			rowData[1] = lista.get(i).getNombre();
			rowData[2] = lista.get(i).getCantidad();
			System.out.println(lista.get(i).getCantidad());
			model.addRow(rowData);
		}
	}
}
