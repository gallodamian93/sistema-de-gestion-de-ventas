package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Util;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.Font;

public class CargandoInicial extends JFrame {

	private JPanel contentPane;
	private Home home;
	private JPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CargandoInicial frame = new CargandoInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CargandoInicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 275);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow][]"));
		setUndecorated(true);
		
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, "cell 0 0,grow");
		
		JLabel lblIniciandoElSistema = new JLabel("Iniciando el sistema ...");
		lblIniciandoElSistema.setFont(new Font("Futura Bk BT", Font.PLAIN, 16));
		contentPane.add(lblIniciandoElSistema, "cell 0 1");
		loadCargando();
		setVisible(true);
		
		Boolean success = Util.initEmf("forrajeriadb");
		if (success) {
			home = new Home();
			dispose();
		} else {
			JOptionPane.showMessageDialog(null, "Error fatal al iniciar el sistema");
			System.exit(0);
		}
	}
	
	private void loadCargando() {
		try {
			panel.add(new JLabel(new ImageIcon("img/loading.gif")));
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

}
